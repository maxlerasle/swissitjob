# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161230154510) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pgcrypto"

  create_table "job_subscriptions", id: :uuid, default: "gen_random_uuid()", force: :cascade do |t|
    t.string   "email"
    t.string   "keywords"
    t.string   "city"
    t.integer  "frequency"
    t.datetime "last_sending"
    t.datetime "next_sending"
    t.string   "locale"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.text     "skills"
    t.string   "city"
    t.string   "canton"
    t.boolean  "remote",        default: false
    t.integer  "min_salary"
    t.integer  "max_salary"
    t.text     "postulation"
    t.string   "company_name"
    t.text     "company_about"
    t.string   "company_url"
    t.string   "company_mail"
    t.boolean  "from_so",       default: false
    t.string   "so_url"
    t.boolean  "active",        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.string   "technologies",  default: [],    array: true
  end

  add_index "jobs", ["slug"], name: "index_jobs_on_slug", unique: true, using: :btree

end
