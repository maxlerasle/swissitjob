class CreateJobSubscriptions < ActiveRecord::Migration
  enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')

  def change
    create_table :job_subscriptions, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.string   :email
      t.string   :keywords
      t.string   :city
      t.integer  :frequency
      t.datetime :last_sending
      t.datetime :next_sending
      t.string   :locale

      t.timestamps
    end
  end
end
