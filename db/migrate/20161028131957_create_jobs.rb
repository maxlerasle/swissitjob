class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string  :title
      t.text    :description
      t.text    :skills
      t.string  :city
      t.string  :canton
      t.boolean :remote, default: false
      t.integer :min_salary
      t.integer :max_salary
      t.text    :postulation
      t.string  :company_name
      t.text    :company_about
      t.string  :company_url
      t.string  :company_mail
      t.boolean :from_so, default: false
      t.string  :so_url
      t.boolean :active, default: false
      t.string  :cat_1
      t.string  :cat_2
      t.string  :cat_3
      t.string  :cat_4
      t.string  :cat_5

      t.timestamps
    end
  end
end
