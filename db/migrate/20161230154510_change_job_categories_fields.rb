class ChangeJobCategoriesFields < ActiveRecord::Migration
  def up
    add_column :jobs, :technologies, :string, array: true, default: []
    Job.all.each do |job|
      job.update_attributes(technologies: [job.cat_1, job.cat_2, job.cat_3, job.cat_4, job.cat_5].compact)
    end
    remove_column :jobs, :cat_1
    remove_column :jobs, :cat_2
    remove_column :jobs, :cat_3
    remove_column :jobs, :cat_4
    remove_column :jobs, :cat_5
  end

  def down
    add_column :jobs, :cat_1, :string
    add_column :jobs, :cat_2, :string
    add_column :jobs, :cat_3, :string
    add_column :jobs, :cat_4, :string
    add_column :jobs, :cat_5, :string
    Job.all.each do |job|
      job.update_attributes(
        cat_1: job.technologies[0],
        cat_2: job.technologies[1],
        cat_3: job.technologies[2],
        cat_4: job.technologies[3],
        cat_5: job.technologies[4],
      )
    end
    remove_column :jobs, :technologies
  end
end
