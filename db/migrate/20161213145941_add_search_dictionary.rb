class AddSearchDictionary < ActiveRecord::Migration
  def up
    execute 'CREATE TEXT SEARCH DICTIONARY custom_synonyms_for_keywords ( TEMPLATE = thesaurus, DictFile = synonyms_for_keywords, Dictionary = english_stem );'
    execute 'CREATE TEXT SEARCH CONFIGURATION simple_syns_keywords (copy=simple);'
    execute 'ALTER TEXT SEARCH CONFIGURATION simple_syns_keywords ALTER MAPPING FOR blank WITH custom_synonyms_for_keywords, simple;'
  end

  def down
    execute 'DROP TEXT SEARCH CONFIGURATION IF EXISTS simple_syns_keywords;'
    execute 'DROP TEXT SEARCH DICTIONARY IF EXISTS custom_synonyms_for_keywords;'
  end
end
