# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://swissitjob.ch"

SitemapGenerator::Sitemap.create do
  add terms_path
  add privacy_path
  add contact_path

  add jobs_path
  Job.active.each do |job|
    add job_path(job.slug, locale: :fr), lastmod: job.updated_at
  end
end