# Files in the config/locales directory are used for internationalization
# and are automatically loaded by Rails. If you want to use locales other
# than English, add the necessary files in this directory.
#
# To use the locales, use `I18n.t`:
#
#     I18n.t 'hello'
#
# In views, this is aliased to just `t`:
#
#     <%= t('hello') %>
#
# To use a different locale, set it with `I18n.locale`:
#
#     I18n.locale = :es
#
# This would use the information in config/locales/es.yml.
#
# To learn more, please read the Rails Internationalization guide
# available at http://guides.rubyonrails.org/i18n.html.

en:
  date:
    today: Today
    formats:
      day_month_year_concise: '%b %d, %Y'
      day_month_concise: '%b %d'
    abbr_month_names: [~, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec]
  canton:
    select: Please select the canton
    appenzell_re: Appenzell Ausserrhoden
    appenzell_ri: Appenzell Innerrhoden
    argovie: Aargau
    berne: Bern
    bale_campagne: Basel-Landschaft
    bale_ville: Basel-Stadt
    fribourg: Fribourg
    geneve: Geneva
    glaris: Glarus
    grisons: Graubünden
    jura: Jura
    lucerne: Luzern
    neuchatel: Neuchâtel
    nidwald: Nidwalden
    obwald: Obwalden
    saint_gall: St. Gallen
    schaffhouse: Schaffhausen
    schwytz: Schwyz
    soleure: Solothurn
    tessin: Ticino
    thurgovie: Thurgau
    uri: Uri
    valais: Valais
    vaud: Vaud
    zoug: Zug
    zurich: Zürich
  views:
    pagination:
      first: "&laquo; First"
      last: "Last &raquo;"
      previous: "&lsaquo; Prev"
      next: "Next &rsaquo;"
      truncate: "&hellip;"
  activerecord:
    errors:
      models:
        job:
          attributes:
            title:
              blank: Title must be filled in.
            city:
              blank: Please tell us where your office is.
            description:
              blank: Are you kidding? No description for your job?
            postulation:
              blank: Please indicate how people can reach you.
            company_name:
              blank: Please fill in your company's name.
            company_url:
              blank: By providing a website URL, candidates will know more about your company.
            company_mail:
              blank: To receive your email confirmation, you have to fill in this field with a valid email adress.
              invalid: Please provide a valid email address.
        job_subscription:
          attributes:
            email:
              blank: Please tell us where you want to receive your job alert.
              invalid: Please provide a valid email address.
            keywords:
              blank: You have to choose at least one keyword for you job alert.
  activemodel:
    errors:
      models:
        message:
          attributes:
            name:
              blank: Please fill in your fullname
            email:
              blank: Please fill in your email address.
              invalid: Please provide a valid email address.
            content: 
              blank: You can not send an empty message.
  pages:
    contact: Contact
    terms: Terms
    privacy: Privacy
  contact:
    page_title: Contact us
    desc_1: You have a question? You want to give us your feedback?
    desc_2: Please fill in the form below to contact us.
    name: Fullname
    mail: Email
    content: Your message
    send_message: Send message
    delivery_error: An error occurred while delivering this message.
    delivery_success: Thank you for your message. We will treat your request shortly.
  mailer:
    subject: Your job listing is online
  title:
    contact: Swiss IT Job | Contact
    show: Swiss IT Job | %{job_title}
    new_ad: Swiss IT Job | New Ad
    preview: Swiss IT Job | Preview
    job_subscription: Swiss IT Job | Job alert
    terms: Swiss IT Job | Terms
    privacy: Swiss IT Job | Privacy
  jobs:
    no_results:
      title: No jobs found
      info: Edit your search and add more keywords to find more jobs.
      # info: Please edit your search or create a job alert to get notified by email when we will have jobs matching your search criteria.
    new: Post a job
    search_keywords: Keywords
    search_city: Location
    preview_button: Preview your ad
    form:
      info: Your job listing will remain on this site for 60 days. After that, your job listing will expire and be removed.
      errors: Some fields are missing or incorrect. Please check that you correctly filled in fields title, city, job description, how to apply, company name, url and email.
      title: 'Step 1: Create your ad'
      subtitle_2: 2. About the position
      subtitle_1: 1. About your company
      title_placeholder: '"Network and System Engineer", "Front-End Developer"'
      city_placeholder: '"Lausanne", "Zürich"'
      remote_placeholder: Allow remote?
      min_salary_placeholder: "75000"
      max_salary_placeholder: "85000"
      min_salary_help: Min salary
      max_salary_help: Max salary
      postulation_placeholder: "Example: Send a resume to johndoe@company.com"
      company_name_placeholder: Enter your company's name
      company_url_placeholder: 'https://mycompany.com'
      company_mail_placeholder: We'll send you a confirmation to this email address.
      cat_1_placeholder: Ruby
      cat_2_placeholder: Rails
      cat_3_placeholder: Angular JS
      cat_4_placeholder: Postgresql
      cat_5_placeholder: Git
      title_help: The title is your first chance to be noticed. Choose a title both concise and relevant. Generic title like Software engineer are often ignored.
      salary_help: Salary is one of the first things developers look at when evaluating a job opportunity. Fill in these fields will improve your chances to seduce candidates.
      technologies_help: By listing the main technologies, your job listing will be much more attractive for candidates. Furthermore, these keywords will be used in our search results. Choose the ones you want to be well positionned on the results.
      description_help: Why would the candidate apply for this job? Will he get to work on a revolutionnary project? Of course, include responsibilities but don't forget to sell candidates on the position.
      skills_help: Be explicit but realistic. Think to separate the 'must have' and the 'nice to have'.
      about_help: What makes your company unique and worth working for? 13nth month of salary, company car, Ski days...
    city: City
    all_cities: All cities
    title: Job Title
    canton: Canton
    remote: Remote
    remote_authorized: Remote
    salary: Salary (CHF per year)
    description: Job description
    skills: Skills and requirements
    postulation: How to apply for this job?
    company_name: Company name
    company_url: URL
    company_website: Website
    company_about: About the company
    company_mail: Email
    published_on: Posted
    interested: Interested?
    so_contact: Apply via Stack Overflow
    technologies: Technologies / Keywords (max 5)
    payment: Ad payment
    payment_button: Validate
    payment_description: Publication of your job listing for 45 days
    successfully_created: |
      Thank you! Your payment has been accepted and your job listing successfully created.
      If you have any question, feel free to contact us at contact@swissitjob.ch.
    temp_successfully_created: Thank you! Your job listing has been successfully created and will remain on our website during 45 days.
  job_subscription:
    title: Job Alert
    edit_title: Edit Job Alert
    daily: Send daily
    weekly: Send weekly
    monthly: Send monthly
    submit: Create alert
    error: An error occured. Please fill in all the fields to create an alert.
    success: You succesfully registered to job alert. Emails will be sent to %{email}.
    mail_subject: Your last job listings on Swiss IT Job
    job_details_link: View details
    unsubscribe: Unsubscribe
    update_success: Your job alert has been succesfully updated.
    update_failure: An error occured during your job alert update. Please try again.
    unsubscription_success: You succesfully unsubscribed from the job alert.
    unsubscription_failure: An error occured during your unsubscription. Please contact us if the error persists.
  keywords: Ruby, AngularJS, iOS
  back_home: Back home
  validate: Validate
  edit: Edit
  home: Home
  sidebar:
    social: Follow swissitjob.ch
    feedback_question: How can we improve this site for you?
    feedback_link: Your suggestions and ideas
    feedback_help: will greatly help us.