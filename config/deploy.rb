# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'swissitjob'

set :ssh_options, {
  forward_agent: true,
  port: 22,
  user: 'deploy'
}

set :rbenv_type, :deploy
set :rbenv_ruby, '2.3.1'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w(rake gem bundle ruby rails)

set :linked_files, %w(config/database.yml)
set :linked_dirs, %w(log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system)

set :deploy_user, :deploy
set :pty, false
set :scm, :git
set :repo_url, 'git@bitbucket.org:mlerasle/swissitjob.git'
set :deploy_to, '/home/deploy/swissitjob'

set :rollbar_token, '5ddd686325cb4caf9caa8b059118885a'
set :rollbar_env, Proc.new { fetch :stage }
set :rollbar_role, Proc.new { :app }

namespace :figaro do
  desc 'SCP transfer figaro configuration to the shared folder'
  task :setup do
    on roles(:app) do
      upload! 'config/application.yml', "#{shared_path}/application.yml", via: :scp
      upload! 'config/secrets.yml', "#{shared_path}/secrets.yml", via: :scp
    end
  end

  desc 'Symlink application.yml to the release path'
  task :symlink do
    on roles(:app) do
      execute "ln -sf #{shared_path}/application.yml #{release_path}/config/application.yml"
      execute "ln -sf #{shared_path}/secrets.yml #{release_path}/config/secrets.yml"
    end
  end
end

namespace :deploy do
  before :updated, 'figaro:setup'
  before :updated, 'figaro:symlink'
  
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
end
