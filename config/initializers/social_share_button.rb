SocialShareButton.configure do |config|
  config.allow_sites = %w(twitter linkedin facebook google_plus)
end
