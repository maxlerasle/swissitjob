# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, 'log/cron_log.log'
env :PATH, ENV['PATH']
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every 1.day, at: '04:30am' do
  rake 'jobs:get_from_so'
  rake 'jobs:deactivate_old'
end

every 2.days, at: '05:00am' do
  rake 'jobs:delete_inactive'
end

every 1.day, at: '5:30am' do
  rake '-s sitemap:refresh'
end

every 1.day, at: '10:30am' do
  rake 'jobs:send_job_alert_emails'
end
