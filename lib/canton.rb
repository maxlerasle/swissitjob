# Set all canton in Switzerland
class Canton
  def self.all
    [
      I18n.t('canton.appenzell_re'),
      I18n.t('canton.appenzell_ri'),
      I18n.t('canton.argovie'),
      I18n.t('canton.berne'),
      I18n.t('canton.bale_campagne'),
      I18n.t('canton.bale_ville'),
      I18n.t('canton.fribourg'),
      I18n.t('canton.geneve'),
      I18n.t('canton.glaris'),
      I18n.t('canton.grisons'),
      I18n.t('canton.jura'),
      I18n.t('canton.lucerne'),
      I18n.t('canton.neuchatel'),
      I18n.t('canton.nidwald'),
      I18n.t('canton.obwald'),
      I18n.t('canton.saint_gall'),
      I18n.t('canton.schaffhouse'),
      I18n.t('canton.schwytz'),
      I18n.t('canton.soleure'),
      I18n.t('canton.tessin'),
      I18n.t('canton.thurgovie'),
      I18n.t('canton.uri'),
      I18n.t('canton.valais'),
      I18n.t('canton.vaud'),
      I18n.t('canton.zoug'),
      I18n.t('canton.zurich')
    ]
  end
end
