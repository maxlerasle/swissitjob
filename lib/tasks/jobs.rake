# Rake tasks for jobs workers
namespace :jobs do
  desc 'Fetch and update jobs from Stack Overflow'
  task get_from_so: :environment do
    FetchSoJobsWorker.perform_async
  end

  desc 'Deactivate old jobs'
  task deactivate_old: :environment do
    DeactivateJobsWorker.perform_async
  end

  desc 'Delete inactive jobs'
  task delete_inactive: :environment do
    DeleteInactiveJobsWorker.perform_async
  end

  desc 'Send job alert emails'
  task send_job_alert_emails: :environment do
    JobAlertEmailWorker.perform_async
  end
end
