# Define skills for tokens search
class Skills
  def self.all
    ['.net', 'actionscript', 'agile', 'ajax', 'android', 'angularjs', 'angular', 'apache', 'asp.net', 'azure', 'baas', 'backbone.js', 'backend', 'bash', 'bdd', 'bigdata', 'bootstrap', 'c', 'c#', 'c++', 'cakephp', 'cassandra', 'cisco', 'cloud', 'cocoa', 'coffeescript', 'css', 'debian', 'devops', 'django', 'docker', 'doctrine', 'dreamwaver', 'drupal', 'e-commerce', 'eclipse', 'elasticsearch', 'ember.js', 'excel', 'express', 'ext-js', 'flash', 'flask', 'frontend', 'git', 'github', 'go', 'hadoop', 'hibernate', 'html', 'illustrator', 'ios', 'java', 'java-ee', 'javascript', 'jenkins', 'joomla', 'jquery', 'json', 'lamp', 'laravel', 'ldap', 'less', 'linux', 'magento', 'markdown', 'mongodb', 'mvc', 'mysql', 'nginx', 'networking', 'node.js', 'node', 'nodejs', 'nosql', 'objective-c', 'office', 'openmp', 'oracle', 'perl', 'phonegap', 'photoshop', 'php', 'play', 'postgresql', 'prestashop', 'puppet', 'python', 'qt', 'rabbitmq', 'rails', 'react', 'reactjs', 'redis', 'redux', 'responsive', 'rest', 'ruby', 'ruby-on-rails', 'sass', 'scala', 'scrum', 'security', 'selenium', 'sem', 'seo', 'shell', 'smarty', 'smo', 'soap', 'spring', 'sql', 'sql-server', 'struts', 'svg', 'svn', 'swift', 'symfony', 'tdd', 'tomcat', 'twig', 'typescript', 'ubuntu', 'ui', 'uml', 'unix', 'ux', 'varnish', 'vmware', 'webservices', 'windows', 'wordpress', 'wpf', 'xcode', 'xml', 'zend']
  end

  def self.colors
    {
      '.net' => '',
      'actionscript' => '',
      'agile' => '',
      'ajax' => '',
      'android' => '',
      'angularjs' => '',
      'angular' => '',
      'apache' => '',
      'asp.net' => '',
      'azure' => '',
      'baas' => '',
      'backbone.js' => '',
      'backend' => '',
      'bash' => '',
      'bdd' => '',
      'bigdata' => '',
      'bootstrap' => '',
      'c' => '',
      'c#' => '',
      'c++' => '',
      'cakephp' => '',
      'cassandra' => '',
      'cisco' => '',
      'cloud' => '',
      'cocoa' => '',
      'coffeescript' => '',
      'css' => '',
      'debian' => '',
      'devops' => '',
      'django' => '',
      'docker' => '',
      'doctrine' => '',
      'dreamwaver' => '',
      'drupal' => '',
      'e-commerce' => '',
      'eclipse' => '',
      'elasticsearch' => '',
      'ember.js' => '',
      'excel' => '',
      'express' => '',
      'ext-js' => '',
      'flash' => '',
      'flask' => '',
      'frontend' => '',
      'git' => '',
      'github' => '',
      'go' => '',
      'hadoop' => '',
      'hibernate' => '',
      'html' => '',
      'illustrator' => '',
      'ios' => '',
      'java' => '',
      'java-ee' => '',
      'javascript' => '',
      'jenkins' => '',
      'joomla' => '',
      'jquery' => '',
      'json' => '',
      'lamp' => '',
      'laravel' => '',
      'ldap' => '',
      'less' => '',
      'linux' => '',
      'magento' => '',
      'markdown' => '',
      'mongodb' => '',
      'mvc' => '',
      'mysql' => '',
      'nginx' => '',
      'networking' => '',
      'node.js' => '',
      'node' => '',
      'nodejs' => '',
      'nosql' => '',
      'objective-c' => '',
      'office' => '',
      'openmp' => '',
      'oracle' => '',
      'perl' => '',
      'phonegap' => '',
      'photoshop' => '',
      'php' => '',
      'play' => '',
      'postgresql' => '',
      'prestashop' => '',
      'puppet' => '',
      'python' => '',
      'qt' => '',
      'rabbitmq' => '',
      'rails' => '',
      'react' => '',
      'reactjs' => '',
      'redis' => '',
      'redux' => '',
      'responsive' => '',
      'rest' => '',
      'ruby' => '',
      'ruby-on-rails' => '',
      'sass' => '',
      'scala' => '',
      'scrum' => '',
      'security' => '',
      'selenium' => '',
      'sem' => '',
      'seo' => '',
      'shell' => '',
      'smarty' => '',
      'smo' => '',
      'soap' => '',
      'spring' => '',
      'sql' => '',
      'sql-server' => '',
      'struts' => '',
      'svg' => '',
      'svn' => '',
      'swift' => '',
      'symfony' => '',
      'tdd' => '',
      'tomcat' => '',
      'twig' => '',
      'typescript' => '',
      'ubuntu' => '',
      'ui' => '',
      'uml' => '',
      'unix' => '',
      'ux' => '',
      'varnish' => '',
      'vmware' => '',
      'webservices' => '',
      'windows' => '',
      'wordpress' => '',
      'wpf' => '',
      'xcode' => '',
      'xml' => '',
      'zend' => ''
    }
  end
end
