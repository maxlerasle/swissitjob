source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.7.1'
# Use sqlite3 as the database for Active Record
# gem 'sqlite3'
# gem 'mysql2'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Views
gem 'font-awesome-rails'
gem 'foundation-rails'
gem 'foundation_rails_helper'
gem 'friendly_id' # Nice urls
gem 'haml'
gem 'jquery-turbolinks'
gem 'kaminari' # Pagination
gem 'recaptcha', require: 'recaptcha/rails'
gem 'social-share-button'
gem 'trix' # Text editor

# Http and html parser
gem 'httparty'
gem 'nokogiri'

# Cron
gem 'whenever', require: false

# Background jobs
gem 'sidekiq'

# DB
gem 'pg'
gem 'pg_search'

# ENV variables
gem 'figaro'

# Payment
gem 'stripe'

# Sitemap
gem 'sitemap_generator'

# Errors
gem 'rollbar'

# Emails
gem 'mailgun-ruby', '~>1.1.2'

# Code analyzer
gem 'rubocop', require: false

# Perf
gem 'newrelic_rpm'

# JSON API
gem 'api-pagination'
group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem 'byebug'
end

group :development do
  gem 'brakeman', require: false
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-bundler', '~> 1.2'
  gem 'capistrano-passenger'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-rbenv', '~> 2.0'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'rack-cors', :require => 'rack/cors'
end
