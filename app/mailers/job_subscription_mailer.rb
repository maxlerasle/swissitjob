# Job alert mailer
class JobSubscriptionMailer < ActionMailer::Base
  default from: 'Swiss IT Job <hello@mail.swissitjob.ch>'

  def alert(job_subscription_id, job_ids)
    @job_subscription = JobSubscription.find(job_subscription_id)
    @jobs = Job.where(id: job_ids)
    I18n.with_locale(@job_subscription.locale) do
      mail(to: @job_subscription.email, subject: I18n.t('job_subscription.mail_subject'))
    end
  end
end
