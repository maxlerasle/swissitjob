# Contact form mailer
class MessageMailer < ActionMailer::Base
  default from: 'Formulaire de contact <hello@mail.swissitjob.ch>'
  default to: 'Maxime <maxlerasle@gmail.com>'

  def new_message(message)
    @message = message
    mail subject: "Message de #{message.name}"
  end
end
