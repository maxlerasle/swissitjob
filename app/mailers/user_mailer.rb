# Confirmation mail after job creation
class UserMailer < ActionMailer::Base
  default from: 'Swiss IT Job <contact@swissitjob.ch>'

  def job_confirmation(job_id)
    @job = Job.find(job_id)
    mail(to: @job.company_mail, subject: I18n.t('mailer.subject'))
  end
end
