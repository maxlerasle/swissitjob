$(document).on("turbolinks:load", function() {
  // ----------------------- 1. JOBS LIST ------------------------------- //
  // Manage tokens in search field
  if ( skills == null ) {
    var skills = [];
  }
  $('#search').tokenize({
    placeholder: "Ruby, AngularJS, iOS",
    onAddToken: function(value, text, e) {
      if ( $.inArray(value, skills) == -1 ) {
        skills.push(value);
        $('#job_subscription_keywords').tokenize().tokenAdd(value, value);
        $.ajax({
          url: $('#search').data('url'),
          method: 'GET',
          dataType: 'script',
          data: { search: skills.join(' ') },
          cache: true
        });
      }
    },
    onRemoveToken: function(value, text,e) {
      skills.splice( $.inArray(value,skills), 1 );
      $('#job_subscription_keywords').tokenize().tokenRemove(value);
      $.ajax({
        url: $('#search').data('url'),
        method: 'GET',
        dataType: 'script',
        data: { search: skills.join(' ') },
        cache: true
      });
    }
  });

  if ( $('#search').data('value') != null ) {
    skills = $('#search').data('value').split(' ');
  }
  $.each(skills, function( index, value ) {
    $('#search').tokenize().tokenAdd(value, value);
  });

  // BUG fix -> ugly but hey, that's jquery
  var count = $('.skills .Tokenize').length;
  if (count > 1) {
    $('.skills .Tokenize').not(':first').remove();
  }
  // ----------------------- 1. END JOBS LIST ------------------------------- //

  // ----------------------- 2. NEW FORM ------------------------------- //
  // Tokenize skills field
  $('#job_technologies').tokenize({
    placeholder: "Ruby, AngularJS, iOS",
    maxElements: 5
  });

  $('trix-editor[input="job_description_trix_input_job"]').on('keydown', function(e) {
    var keyCode = e.keyCode;
    if (keyCode == 9) { 
      e.preventDefault(); 
      $('trix-editor[input="job_skills_trix_input_job"]').focus();
    }
  });

  $('#job_company_url').on('keydown', function(e) {
    var keyCode = e.keyCode;
    if (keyCode == 9) { 
      e.preventDefault(); 
      $('trix-editor[input="job_company_about_trix_input_job"]').focus();
    }
  });

  // Set trix editor border color on focus and focusout
  $('trix-editor').on("focus", function() {
    $(this).css('border', '1px solid #2199e8')
  });

  $('trix-editor').on("focusout", function() {
    $(this).css('border', '1px solid #cacaca')
  });

  // Set inline help
  // Title field help
  $("input#job_title").on("focus", function() {
    $('.title_help').removeClass("hide");
    var pos = $('input#job_title').offset().top
    $('.title_help').offset({ top: pos });
  });

  $("input#job_title").on("focusout", function() {
    $('.title_help').addClass("hide");
  });
  
  // Salary fields help
  $("input#job_min_salary, input#job_max_salary").on("focus", function() {
    $('.salary_help').removeClass("hide");
    var pos = $('input#job_min_salary').offset().top
    $('.salary_help').offset({ top: pos });
  });

  $("input#job_min_salary, input#job_max_salary").on("focusout", function() {
    $('.salary_help').addClass("hide");
  });

  // Technologies fields help managed in jquery.tokenize.js

  // Description field help
  var description_field = $('trix-editor[input="job_description_trix_input_job"]');
  description_field.on('focus', function() {
    $('.description_help').removeClass("hide");
    var pos = description_field.offset().top
    $('.description_help').offset({ top: pos });
  });

  description_field.on('focusout', function() {
    $('.description_help').addClass("hide");
  });

  // Skills field list
  var skills_field = $('trix-editor[input="job_skills_trix_input_job"]');
  skills_field.on('focus', function() {
    $('.skills_help').removeClass("hide");
    var pos = skills_field.offset().top
    $('.skills_help').offset({ top: pos });
  });
  skills_field.on('focusout', function() {
    $('.skills_help').addClass("hide");
  });

  // About company field
  var about_field = $('trix-editor[input="job_company_about_trix_input_job"]');
  about_field.on('focus', function() {
    $('.company_about_help').removeClass("hide");
    var pos = about_field.offset().top
    $('.company_about_help').offset({ top: pos });
  });
  about_field.on('focusout', function() {
    $('.company_about_help').addClass("hide");
  });

  // Error fields
  $('small.error').each(function() {
    $(this).removeClass('error').addClass('form-error is-visible');
  });

  $('input.error').each(function() {
    $(this).removeClass('error').addClass('is-invalid-input');
  });

  $('textarea.error').each(function() {
    $(this).removeClass('error').addClass('is-invalid-input');
  });
  // ----------------------- 2. END NEW FORM ---------------------------- //
});