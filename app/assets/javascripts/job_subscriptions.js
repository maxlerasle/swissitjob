$(document).on("turbolinks:load", function() {
  // Manage tokens in new subscription modal
  if ( skills == null ) {
    var skills = [];
  }

  $('#job_alert #job_subscription_keywords').tokenize({
    placeholder: "Ruby, AngularJS, iOS"
  });

  if ( $('#search').data('value') != null ) {
    skills = $('#search').data('value').split(' ');
  }
  $.each(skills, function( index, value ) {
    $('#job_alert #job_subscription_keywords').tokenize().tokenAdd(value, value);
  });

  // Manage tokens in edit view
  if ( tokens == null ) {
    var tokens = [];
  }
  $('#job_alert_edit #job_subscription_keywords').tokenize({
    placeholder: "Ruby, AngularJS, iOS"
  });

  if ( $('#job_alert_edit #job_subscription_keywords').data('tokens') != null ) {
    var tokens = $('#job_alert_edit #job_subscription_keywords').data('tokens').split(' ');
  }
  $.each(tokens, function( index, value ) {
    $('#job_alert_edit #job_subscription_keywords').tokenize().tokenAdd(value,value);
  });
})