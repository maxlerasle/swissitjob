# Manage job subscriptions
class JobSubscription < ActiveRecord::Base
  before_validation :format_keywords
  before_create :set_next_sending, :set_last_sending

  validates_presence_of :email, :keywords, :frequency
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }

  scope :today, -> { where('DATE(next_sending) = ?', Date.today) }

  def set_next_sending
    self.next_sending = 
      if frequency == 1
        Time.now + 1.day
      elsif frequency == 2
        Time.now + 1.week
      else
        Time.now + 1.month
      end
  end

  private

  def set_last_sending
    self.last_sending = Time.now
  end

  def format_keywords
    return true unless keywords_changed?
    self.keywords = JSON.parse(keywords).join(' ').lstrip
    self.keywords = nil if self.keywords == ''
  end
end
