# Job model
class Job < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  include FriendlyId
  friendly_id :slug_candidates, use: :slugged
  before_save :clean_up_technologies

  validates_presence_of :title, :city, :description, :company_name
  validates_presence_of :company_url, :company_mail, :postulation, if: :job_from_site?
  validates :company_mail, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, if: :job_from_site?

#   include PgSearch
#   pg_search_scope :search,
#                   against: {
#                     technologies:  'A',
#                     title:         'B',
#                     description:   'C',
#                     skills:        'D'# ,
# #                     company_about: 'D',
# #                     company_name:  'D',
# #                     city:          'D',
# #                     canton:        'D'
#                   },
#                   using: { tsearch: { any_word: true, dictionary: 'simple_syns_keywords' } }

  scope :active, -> { where(active: true) }
  scope :ordered, -> { order('from_so, created_at DESC') }
  # scope :for_search, ->(search) { search(search) if search.present? }
  scope :for_city, ->(city) { where(city: city) if city.present? }
  scope :created_after, ->(datetime) {
    where('(from_so = ? AND created_at >= ?) OR (from_so = ? AND created_at >= ?)', true, datetime.beginning_of_day, false, datetime)
  }

  def self.for_search(search)
    return all if search.blank?
    search_terms = search.strip.split.compact.reject { |t| t == '' }
    if search_terms[0].casecmp('java') == 0
      query = "'#{search_terms[0]}' = ANY(technologies) OR title ilike '%#{search_terms[0]}(?!s|S)%' OR description ilike '%#{search_terms[0]}(?!s|S)%' OR skills ilike '%#{search_terms[0]}(?!s|S)%'"
    elsif search_terms[0].casecmp('c') == 0 || search_terms[0].casecmp('r') == 0
      query = "'#{search_terms[0]}' = ANY(technologies)"
    else
      query = "'#{search_terms.first}' = ANY(technologies) OR title ilike '%#{search_terms.first}%' OR description ilike '%#{search_terms.first}%' OR skills ilike '%#{search_terms.first}%'"
    end
    1.upto(search_terms.size - 1) do |i|
      if search_terms[i].casecmp('java') == 0
        query += " OR '#{search_terms[i]}' = ANY(technologies) OR title ilike '%#{search_terms[i]}(?!s|S)%' OR description ilike '%#{search_terms[i]}(?!s|S)%' OR skills ilike '%#{search_terms[i]}(?!s|S)%'"
      elsif search_terms[i].casecmp('c') == 0 || search_terms[i].casecmp('r') == 0
        query += " OR '#{search_terms[i]}' = ANY(technologies)"
      else
        query += " OR '#{search_terms[i]}' = ANY(technologies) OR title ilike '%#{search_terms[i]}%' OR description ilike '%#{search_terms[i]}%' OR skills ilike '%#{search_terms[i]}%'"
      end
    end if search_terms.size > 1
    where(query)
  end

  def self.cities
    active.pluck(:city).uniq.sort
  end

  def slug_candidates
    [
      [:title, :city],
      [:title, :city, :company_name],
      [:title, :city, :company_name, :id]
    ]
  end

  def job_from_site?
    !from_so
  end

  def safe_description
    return description.html_safe unless from_so
    description.gsub('</li><br />', '</li>').gsub('<br /><ul>', '<ul>').html_safe
  end

  def safe_skills
    return skills.html_safe unless from_so
    skills.gsub('</li><br />', '</li>').gsub('<br /><ul>', '<ul>').html_safe
  end

  def safe_about
    return company_about.html_safe unless from_so
    company_about.gsub('</li><br />', '</li>').gsub('<br /><ul>', '<ul>').html_safe
  end

  def days_from_creation
    return I18n.t('date.today') unless created_at
    I18n.l(created_at.to_date, format: :day_month_year_concise)
  end

  def salary
    return nil unless min_salary || max_salary
    if min_salary && !max_salary
      "CHF #{number_with_delimiter(min_salary)}"
    elsif max_salary && !min_salary
      "CHF #{number_with_delimiter(max_salary)}"
    else
      "CHF #{number_with_delimiter(min_salary)} - #{number_with_delimiter(max_salary)}"
    end
  end

  private

  def clean_up_technologies
    self.technologies = self.technologies.compact.reject { |t| t == "" }
  end
end
