# Worker for inactive jobs deletion
class DeleteInactiveJobsWorker
  include Sidekiq::Worker

  def perform
    Job.where(active: false).where('created_at < ?', 2.days.ago).delete_all
  end
end
