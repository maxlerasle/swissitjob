# Worker to get jobs listing from SO
class FetchSoJobsWorker
  include HTTParty
  include Sidekiq::Worker

  def perform
    so_response = HTTParty.get('https://stackoverflow.com/jobs/feed?location=Switzerland')
    jobs = Hash.from_xml(so_response)['rss']['channel']['item']
    existing_urls = []

    jobs.each do |job|
      existing_urls << job['link']
      local_job = Job.where(so_url: job['link']).first
      next if local_job.present? && local_job.active && local_job.created_at == job['pubDate']
      if local_job.present? && local_job.created_at != job['pubDate']
        local_job.update_attributes(created_at: job['pubDate'])
        next
      end
      job_url = job['link']
      job_web_page = HTTParty.get(job_url)
      job_html = Nokogiri::HTML(job_web_page) do |config|
        config.noblanks
      end

      # Parsing html
      if job_html.css('.salary')[0]
        salary = job_html.css('.salary')[0].text.gsub("\r", '').gsub("\n", '').strip
        if salary != 'Provides Equity'
          if salary.starts_with?('$')
            salary = salary[1..-1]
            min_salary = salary.split[0].gsub(',', '').to_i if salary.split[0]
            max_salary = salary.split[2].gsub(',', '').to_i if salary.split[2]
          else
            min_salary = salary.split[1].gsub(',', '').to_i if salary.split[1]
            max_salary = salary.split[3].gsub(',', '').to_i if salary.split[3]
          end
        end
      end
      job_description = job_html.css('.jobdetail .description')[0].css('div').to_s.gsub("\r", '').gsub("\n", '')
      # Check if there is a Skills & requirements section
      section_2 = job_html.css('.jobdetail .description')[1]
      if section_2 && section_2.previous_element.text.gsub("\r", '').gsub("\n", '').strip.start_with?('Skills')
        job_skills = job_html.css('.jobdetail .description')[1].to_s.gsub("\r", '').gsub("\n", '')
        info_company = job_html.css('.jobdetail .description')[2].to_s.gsub("\r", '').gsub("\n", '')
      elsif section_2 && section_2.previous_element.text.gsub("\r", '').gsub("\n", '').strip.start_with?('About')
        info_company = job_html.css('.jobdetail .description')[1].to_s.gsub("\r", '').gsub("\n", '')
      end

      # Create the job
      new_job = Job.create(
        title:         job['title'].split(' at ').first,
        description:   job_description,
        skills:        job_skills,
        city:          job['location'].split(',').first,
        company_name:  job['author']['name'],
        company_about: info_company,
        from_so:       true,
        so_url:        job_url,
        min_salary:    min_salary,
        max_salary:    max_salary,
        created_at:    job['pubDate'],
        updated_at:    job['updated'],
        active:        true
      )
      # Handle categories
      if job['category'] && job['category'].is_a?(Array)
        new_job.technologies = job['category']
        # job['category'][0..4].each_with_index do |category, i|
        #   next if category.empty?
        #   new_job.send("cat_#{i + 1}=", category)
        # end
      elsif job['category'] && job['category'].is_a?(String)
        # new_job.cat_1 = job['category']
        new_job.technologies << job['category']
      end
      new_job.save!
    end
    Job.where(from_so: true).each do |job|
      unless existing_urls.include?(job.so_url)
        job.update_attributes(active: false)
      end
    end
  end
end
