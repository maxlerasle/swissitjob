# Worker for contact form email
class ContactEmailWorker
  include Sidekiq::Worker

  def perform(message)
    MessageMailer.new_message(message).deliver
  end
end
