# Worker to send job alert emails
class JobAlertEmailWorker
  include Sidekiq::Worker

  def perform
    JobSubscription.today.each do |subscription|
      jobs = Job.active.created_after(subscription.last_sending).for_search(subscription.keywords)
      subscription.update_attributes(next_sending: subscription.set_next_sending)
      next unless jobs.any?
      JobSubscription.transaction do
        JobSubscriptionMailer.alert(subscription.id, jobs.pluck(:id)).deliver
        subscription.update_attributes(last_sending: Time.now)
      end
    end
  end
end
