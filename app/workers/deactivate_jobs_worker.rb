# Worker for jobs deactivation after 45 days
class DeactivateJobsWorker
  include Sidekiq::Worker

  def perform
    Job.where('created_at < ?', 60.days.ago).update_all(active: false)
  end
end
