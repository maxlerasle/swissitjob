# Worker to send job confirmation email
class JobConfirmationEmailWorker
  include Sidekiq::Worker

  def perform(job_id)
    UserMailer.job_confirmation(job_id).deliver
  end
end
