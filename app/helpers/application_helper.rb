# Define views helpers
module ApplicationHelper
  def meta_description(text)
    content_for(:meta_description) { text }
  end

  def page_title(text)
    content_for(:title) { text }
  end
end
