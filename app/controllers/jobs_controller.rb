# Jobs management
class JobsController < ApplicationController
  before_action :find_job, only: [:show, :edit, :update, :activate]

  def index
    session[:search] = params[:search] if params[:search]
    session[:city] = params[:city] if params[:city]
    session[:city] = nil if params[:city] == 'all'

    # General search
    db_jobs = Job.active.for_city(session[:city]).for_search(session[:search]).ordered
    @jobs = db_jobs.page(params[:page]).per(20)
    respond_to do |format|
      format.html
      format.js
      format.json { paginate json: db_jobs, per_page: 20 }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json { render json: @job }
    end
  end

  def new
    @job = Job.new
  end

  def create
    @job = Job.new(jobs_params)
    if @job.save
      redirect_to job_path(@job)
    else
      flash.now[:error] = t('jobs.form.errors')
      render action: :new
    end
  end

  def edit; end

  def update
    if @job.update_attributes(jobs_params)
      redirect_to job_path(@job)
    else
      flash.now[:error] = t('jobs.form.errors')
      render action: :edit
    end
  end

  def activate
    # charge_error = nil
    # begin
    #   # Amount in cents
    #   @amount = 10000
    #
    #   customer = Stripe::Customer.create(
    #     email:  params[:stripeEmail],
    #     source: params[:stripeToken]
    #   )
    #
    #   charge = Stripe::Charge.create(
    #     customer:      customer.id,
    #     amount:        @amount,
    #     description:   I18n.t("jobs.payment_description"),
    #     currency:      'chf',
    #     receipt_email: @job.company_mail
    #   )
    #
    # rescue Stripe::CardError => e
    #   charge_error = e.message
    # end
    # if charge_error
    #   flash[:error] = charge_error
    #   render action: :edit
    # else
    #   @job.update_attributes(active: true)
    #   flash[:success] = I18n.t("jobs.successfully_created")
    #   # UserMailer.job_confirmation(@job.id).deliver
    #   redirect_to job_path(@job)
    # end

    if @job.update_attributes(active: true)
      flash[:success] = t('jobs.temp_successfully_created')
      JobConfirmationEmailWorker.new.perform(@job.id)
      redirect_to job_path(@job)
    else
      flash.now[:error] = t('jobs.form.errors')
      render action: :edit
    end
  end

  private

  def jobs_params
    params.require(:job).permit(
      :title, :description, :city, :canton, :remote, :min_salary, :max_salary,
      :postulation, :company_name, :company_url, :company_mail, :active,
      { :technologies => [] }, :skills, :company_about, :from_so
    )
  end

  def find_job
    @job = Job.friendly.find(params[:id])
  end
end
