# Manage job subscriptions
class JobSubscriptionsController < ApplicationController
  before_action :find_job_subscription, except: [:create]

  def create
    @job_subscription = JobSubscription.new(job_subscriptions_params)
    @job_subscription.locale = params[:locale]
    respond_to do |format|
      if @job_subscription.save
        flash.now[:success] = t('job_subscription.success', email: @job_subscription.email)
      else
        flash.now[:error] = t('job_subscription.error')
      end
      format.js
    end
  end

  def show
    redirect_to edit_job_subscription_path(@job_subscription)
  end

  def edit; end

  def update
    if @job_subscription.update_attributes(job_subscriptions_params)
      flash.now[:success] = t('job_subscription.update_success')
    else
      flash.now[:error] = t('job_subscription.update_failure')
    end
    render action: :edit
  end

  def destroy
    if @job_subscription.destroy
      flash[:success] = t('job_subscription.unsubscription_success')
      redirect_to locale_root_path
    else
      flash[:error] = t('job_subscription.unsubscription_failure')
      render action: :edit
    end
  end

  private

  def find_job_subscription
    @job_subscription = JobSubscription.find(params[:id])
  end

  def job_subscriptions_params
    params.require(:job_subscription).permit(:email, :city, :frequency, :last_sending, :next_sending, :locale, { :keywords => [] })
  end
end
