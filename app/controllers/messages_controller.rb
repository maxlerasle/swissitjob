# Messages sent through contact form
class MessagesController < ApplicationController
  def new
    @message = Message.new
  end

  def create
    @message = Message.new(message_params)

    if verify_recaptcha(model: @message) && @message.valid?
      ContactEmailWorker.new.perform(@message)
      flash[:success] = t('contact.delivery_success')
      redirect_to contact_path
    else
      flash.now[:error] = t('contact.delivery_error')
      render :new
    end
  end

  private

  def message_params
    params.require(:message).permit(:name, :email, :content)
  end
end
